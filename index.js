// alert('hello')


let student1 = '2020-1923'
console.log(student1)
let student2 = '2020-1924'
console.log(student2)
let student3 = '2020-1925'
console.log(student3)
let student4 = '2020-1926'
console.log(student4)
let student5 = '2020-1927'
console.log(student5)

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927']


/*
	Arrays
		- used to store multiple related values in single variables.
		- declared using square brackets ([]) also known as an 'Array Literals'

		Syntax:
			let/const arrayName = [elementA, elementB... element]
*/

let grades = [98.5, 94.3, 89.2, 90.1]
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

let mixedArr = [12, 'Asus', null, undefined, {}]; //Not Recommended
console.log(mixedArr)

//Each element of an array can also be written in a seperate line
let myTasks = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake sass'
]
console.log(myTasks)

// We can also store the values of separate variables in an array
let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];
console.log(cities)

// Array Length Property
console.log(myTasks.length) //4
console.log(cities.length) //3

let blankArr = [];
console.log(blankArr.length) //result is 0 because the array is empty

let fullName = "Jamie Noble";
console.log(fullName.length) //11

myTasks.length = myTasks.length - 1
console.log(myTasks.length)
console.log(myTasks)

cities.length--
console.log(cities)

//Can we do a delete a character in a string using .length property?

console.log(fullName.length)
fullName.length = fullName.length - 1
console.log(fullName.length)
fullName.length --
console.log(fullName)

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
console.log(theBeatles.length)
theBeatles.length ++
console.log(theBeatles.length)
console.log(theBeatles)

//array[i] = 'new value'
theBeatles[4] = 'Rupert'
console.log(theBeatles)

/*
Accessing Elements of an Array

	Syntax:
		arrayName[index]
*/

console.log(grades[0])
console.log(computerBrands[3])

console.log(grades[20]);

let lakersLegends = ['Kobe', 'Shaq', 'LeBron', 'Magic', 'Kareem']
console.log(lakersLegends[1])
console.log(lakersLegends[3])

//can we save array items in another variable??

let currentLaker = lakersLegends[2]
console.log(currentLaker)

console.log('Arrays before reassignment')
console.log(lakersLegends)
lakersLegends[2] = 'Pau Gasol';
console.log('Arrays after reassignment')
console.log(lakersLegends)

//Accessing the last element of an Array
let bullsLegend = ['Jordan', 'Pippen', 'Rodman','Rose', 'Kukoc']
let lastElementIndex = bullsLegend.length - 1
console.log(bullsLegend[lastElementIndex])

console.log(bullsLegend[bullsLegend.length - 1])

//Adding Items into the Array
let newArr = [];
console.log(newArr);
console.log(newArr[0]);
newArr[0] = 'Jennie';
console.log(newArr);
newArr[1] = 'Jisoo';
console.log(newArr)

//what if we want to add an element at the end of the array?

newArr[newArr.length++] = 'Lisa'
console.log(newArr)
// newArr[3] = 'Daeyun'
// console.log(newArr)  

//Looping over an Array

for (let index = 0; index < newArr.length; index++) {

	console.log(newArr[index])
}

let numArr = [5, 12, 3, 30, 46, 50, 88]

for(let index = 0; index < numArr.length; index++) {

	if(numArr[index] % 5 === 0) {
		console.log(numArr[index] + " is divisible by 5.")
	} else {
		console.log(numArr[index] + ' is not divisible by 5.')
	}
}

//Multidimensional Array
let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard)
console.log(chessBoard[1][4])
console.log('Pawn moves to: ' + chessBoard[7][4])